import 'dart:io';

class Calc {
  // instance fields
  double num1=0;
  double num2=0;
  double sum=0;
  //...
  Calc(double firstNum, double secondNum) {
    this.num1 = firstNum;
    this.num2 = secondNum;
  }
  double add(double firstNum, double secondNum) {
    sum=firstNum + secondNum;
    return sum;
  }

  double subtract(double firstNum, double secondNum) {
    sum=firstNum - secondNum;
    return sum;
  }

  double multiply(double firstNum, double secondNum) {
    sum=firstNum * secondNum;
    return sum;
  }

  double divide(double firstNum, double secondNum) {
    sum=firstNum / secondNum;
    return sum;
  }
}
